import 'package:flutter/material.dart';
import 'dart:math';

class Home extends StatefulWidget {
  const Home({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int leftDie = 1;
  int rightDie = 1;

  void throwDice() {
    setState(() {
      leftDie = Random().nextInt(6)+1;
      rightDie = Random().nextInt(6)+1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
                child: TextButton(
                    onPressed: () => {
                      throwDice(),
                      },
                    child: Image.asset('assets/images/$leftDie.png'))),
            Expanded(
                child: TextButton(
                    onPressed: () => {
                      throwDice(),
                      },
                    child: Image.asset('assets/images/$rightDie.png'))),
          ],
        ),
      ),
    );
  }
}
